import requests
from django.contrib.auth.password_validation import validate_password
from rest_framework.fields import CurrentUserDefault
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from Todo.models import Task
from Users.models import MyUser

class TaskSerializer(serializers.ModelSerializer):
    author = serializers.HiddenField(default=CurrentUserDefault())

    class Meta:
        model = Task
        fields = ('title', 'content', 'date_deadline', 'author')


class TaskSerializerExecute(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ('title', 'is_complete',)
        # fields = '__all__'

class RegisterSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(
        required=True,
         validators=[UniqueValidator(queryset=MyUser.objects.all())]
        )
    password = serializers.CharField(
        write_only=True, required=True, validators=[validate_password])
    password2 = serializers.CharField(write_only=True, required=True)
    class Meta:
        model = MyUser
        fields = ('email', 'password', 'password2',)
    def validate(self, attrs):
        if attrs['password'] != attrs['password2']:
            raise serializers.ValidationError(
                {"password": "Password fields didn't match."})
        return attrs
    def create(self, validated_data):
        user = MyUser.objects.create(
            email=validated_data['email'])
        user.set_password(validated_data['password'])
        user.save()
        return user
from __future__ import absolute_import
import os
from celery import Celery

# этот код скопирован с manage.py
# он установит модуль настроек по умолчанию Django для приложения 'celery'.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'test_work_for_uchet.settings')

# здесь вы меняете имя
app = Celery('test_work_for_uchet', # Название задачи
             broker='redis://localhost',
             backend='redis://localhost')

# Для получения настроек Django, связываем префикс "CELERY" с настройкой celery
app.config_from_object('django.conf:settings', namespace='CELERY')

# загрузка tasks.py в приложение django
app.autodiscover_tasks()

# @app.task(bind=True)
# def send_mail(self):
#     print(f'Сообщение отправлено на почту')
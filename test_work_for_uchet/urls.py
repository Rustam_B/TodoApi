from django.contrib import admin
from django.urls import path, include

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)
from Todo.apiview import TaskApiView, TaskDetaiUpdDelApiView, TaskExecute, RegisterUserAPIView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('api/auth/', include('rest_framework.urls')),
    path('api/todo/', TaskApiView.as_view(), name='todo_api'),
    path('api/todo/<int:pk>/', TaskDetaiUpdDelApiView.as_view(), name='todo_detail_update_del_api'),
    path('api/todo/<int:pk>/execute/', TaskExecute.as_view(), name='todo_execute'),
    path('register/',RegisterUserAPIView.as_view())

]

from django.contrib.auth import get_user_model
from django.db import models
from Users.models import MyUser

class Task(models.Model):
    title = models.CharField(max_length=100, null=False, blank=False, verbose_name='Заголовок')
    content = models.TextField(max_length=1000, null=False, blank=False, verbose_name='Описание')
    date_deadline = models.DateField(max_length=50, null=True, blank=False, verbose_name='Дата исполнения')
    is_complete = models.BooleanField(null=False, verbose_name='Исполнено', default=False)
    author = models.ForeignKey(get_user_model(), on_delete=models.CASCADE, verbose_name='Автор')

    def __str__(self):
        return self.title